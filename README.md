# Elastic on Kubernetes

Pasos sencillos para desplegar ELK en Kubernetes con Kibana

## Install ECK
Ref: https://www.elastic.co/guide/en/cloud-on-k8s/current/k8s-deploy-eck.html

```bash
export ECK_VERSION=1.9.1
kubectl create ns elastic-system
```

## CDRS
```bash
kubectl create -f https://download.elastic.co/downloads/eck/$ECK_VERSION/crds.yaml -n elastic-system
kubectl apply -f https://download.elastic.co/downloads/eck/$ECK_VERSION/operator.yaml -n elastic-system
```

# Crear el cluster de ELastic
```bash
cat <<EOF | kubectl -n elastic-system apply -f -
apiVersion: elasticsearch.k8s.elastic.co/v1
kind: Elasticsearch
metadata:
  name: elastic
  namespace: elastic-system
spec:
  version: 7.16.2
  nodeSets:
  - name: default
    count: 3
    config:
      node.store.allow_mmap: false
    podTemplate:
      spec:
        affinity: {}
EOF
  
# Para ver el status de Elastic debe decir status green
kubectl logs -f statefulset.apps/elastic-operator -n elastic-system
kubectl get elasticsearch -n elastic-system  
kubectl -n elastic-system get pods --selector="elasticsearch.k8s.elastic.co/cluster-name=elastic"

# Abrimos otra terminal y ejecutamos
kubectl port-forward service/elastic-es-http 9200  -n elastic-system

# Regresamos a donde estamos para generar el secreto
PASSWORD=$(kubectl get secret elastic-es-elastic-user -o go-template='{{.data.elastic | base64decode}}' -n elastic-system)
echo $PASSWORD

curl -u "elastic:$PASSWORD" -k "https://localhost:9200"
```

## Install Kibana
 ```bash
cat <<EOF | kubectl -n elastic-system apply -f -
apiVersion: kibana.k8s.elastic.co/v1
kind: Kibana
metadata:
  name: kibana
spec:
  version: 7.16.2
  count: 1
  elasticsearchRef:
    name: elastic
EOF

# Para ver el status de Kibana debe decir status green
kubectl get kibana -n elastic-system
kubectl get pod --selector="kibana.k8s.elastic.co/name=kibana" -n elastic-system   || watch kubectl get pod --selector="kibana.k8s.elastic.co/name=kibana" -n elastic-system
kubectl get service kibana-kb-http -n elastic-system

# En otra terminal 
kubectl port-forward service/kibana-kb-http 5601 -n elastic-system

```

Vamos a la web y nos logeamos con la el usuario `elastic` y la clave generada del `echo $PASSWORD`
